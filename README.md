# Deskripsi Singkat
Bumi diinvasi oleh makhluk angkasa dan sebentar lagi akan dikuasai oleh para Slime!  Capai pesawat ulang-alik untuk menyelamatkan diri menggunakan kekuatan super yang kamu miliki! Bisakah kamu mencapai pesawat hanya dalam waktu 3 menit ?

# Konsep Permainan
Permainan bergenre Platformer yang terinspirasi oleh Syoboncat Super Mario Bros.  dimana pemain harus mencapai objective dengan melewati musuh dan rintangan. Berbeda dengan Super Mario Bros., permainan ini tidak menggunakan sistem nyawa namun memakai time limit dimana pemain bisa mencoba terus hingga waktunya habis. Item dan metode mengalahkan lawan juga dihilangkan untuk menambah tekanan kepada pemain untuk menghindari lawan dan rintangan.

# Cara Bermain
Tombol **Arah Panah** : Untuk bergerak (kiri, kanan dan atas untuk melompat)
Tombol **[Spasi]** : die Welt (kekuatan menghentikan waktu)
Tombol **[R]** : Wiederholen (kekuatan mengulang dari lokasi awal, dipakai saat terkena rintangan atau lawan)
Source Code
Gitlab : https://gitlab.com/Ervan.Adiwijaya/untitled_platformer_game

# Daftar Aset Luar
#### Tutorial-3 dan Tutorial-5 dari mata kuliah Game Development Fasilkom UI 2019/2020 (https://gitlab.com/csui-gamedev/tutorials)
#### Fonts :
* Data Control (by Vic Feiger): https://www.1001freefonts.com/data-control.font
* Karmatic Arcade (by Vic Feiger): https://www.1001freefonts.com/karmatic-arcade.font
* Boycott (by Flat-it) : https://www.1001freefonts.com/boycott.font ; http://flat-it.com