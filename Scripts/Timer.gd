extends Label

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export (NodePath) var getTime
# Called when the node enters the scene tree for the first time.
func _process(delta):
    self.text = "Time Left : " + str(get_node(getTime).gameTime)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#    pass
