extends KinematicBody2D

export (int) var friction = 50
export (int) var speed = 300
export (int) var jump_speed = -600
export (int) var GRAVITY = 1200
export (bool) var is_hurt = false

export var camera_left_padding = 300
onready var camera = get_node("Camera2D")
onready var sprite = get_node("Sprite")

const UP = Vector2(0,-1)

var velocity = Vector2()

func _ready() :
    set_process(true)
    set_physics_process(true)
    
func get_input():
    velocity.x = 0
    if is_on_floor() and Input.is_action_just_pressed('ui_up'):
        velocity.y = jump_speed
    if Input.is_action_pressed('ui_right'):
        velocity.x += speed
    if Input.is_action_pressed('ui_left'):
        velocity.x -= speed

func _physics_process(delta):
    if (self.position.x - camera_left_padding) > camera.limit_left:
        camera.limit_left = self.position.x - camera_left_padding
    velocity.y += delta * GRAVITY
    if !is_hurt :
        get_input()
    velocity = move_and_slide(velocity, UP)

func _process(delta):
    if (is_hurt) :
        camera.current=false
        sprite.play("Hurt")
        GRAVITY=600
        if velocity.x > 0 :
            velocity.x -= friction
        elif velocity.x < 0 :
            velocity.x -= friction
    else :
        if is_on_floor() and velocity.x == 0:
            sprite.play("Idle")
        elif velocity.y != 0 :
            if velocity.y < 0 :
                sprite.play("Jump")
            else :
                sprite.play("Fall")
            if velocity.x > 0 :
                sprite.flip_h = false
            elif velocity.x < 0 : 
                sprite.flip_h = true
        else :
            sprite.play("Walk")
            if velocity.x < 0 :
                sprite.flip_h = true
            else :
                sprite.flip_h = false