extends KinematicBody2D

export (int) var GRAVITY = 2200
export (bool) var isActive = true
var velocity = Vector2()

const UP = Vector2(0,-1)

onready var sprite = get_node("Sprite")

func _ready():
    set_physics_process(true)

func _physics_process(delta):
    if position.y < 0 :
        queue_free()
    if isActive:
        velocity.y = -GRAVITY
    velocity = move_and_slide(velocity, UP)