extends LinkButton

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export (String) var scene_to_load
# Called when the node enters the scene tree for the first time.

func _on_Button_pressed():
    get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))


func _on_Quit_pressed():
    get_tree().quit()
