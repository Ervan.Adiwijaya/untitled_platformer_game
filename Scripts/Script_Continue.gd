extends AcceptDialog

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
    pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#    pass


func _on_AcceptDialog_confirmed():
    global.time=180
    get_tree().change_scene("res://Scenes/Level_X.tscn")


func _on_AcceptDialog_popup_hide():
    _on_AcceptDialog_confirmed()
