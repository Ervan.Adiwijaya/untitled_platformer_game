extends RigidBody2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export(int) var move_speed = 100
export(bool) var isActive = false
var velocity = Vector2()
var player = null
# Called when the node enters the scene tree for the first time.
func _ready():
    pass

func _on_Node2D_body_entered(body):
    if isActive:
        velocity.x = move_speed
    if body.get_name() == "Player" and !body.is_hurt:
        body.is_hurt = true
        player = body
        player.velocity.y = -500