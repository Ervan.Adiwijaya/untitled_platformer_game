extends KinematicBody2D

export (int) var move_speed = 100
export (int) var GRAVITY = 1200
export (bool) var isActive = true
var velocity = Vector2()
var direction = -1
var dir_ctl = -1

const UP = Vector2(0,-1)

onready var sprite = get_node("Sprite")

func _ready():
    set_physics_process(true)
    
func _physics_process(delta):
    if position.y < -300 or position.y > 700 :
        queue_free()
    if is_on_wall():
        direction *= dir_ctl
        if direction > 0 :
            sprite.flip_h=true
        else :
            sprite.flip_h=false
    if is_on_floor():
        velocity.x = (move_speed*direction)
    if isActive:
        velocity.y += delta*GRAVITY
    velocity = move_and_slide(velocity, UP)