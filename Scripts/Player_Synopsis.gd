extends KinematicBody2D

export (bool) var is_hurt = false

export var camera_left_padding = 300
onready var sprite = get_node("Sprite")

const UP = Vector2(0,-1)

var velocity = Vector2()

func _ready() :
    set_process(true)
    set_physics_process(true)

func _physics_process(delta):
    velocity = move_and_slide(velocity, UP)

func _process(delta):
    if (is_hurt) :
        sprite.play("Hurt")
    else:
        sprite.play("Walk")