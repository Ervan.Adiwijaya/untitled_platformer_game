extends Area2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export (NodePath) var enemies

# Called when the node enters the scene tree for the first time.
func _ready():
    pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#    pass


func _on_Spawn_Trap_body_entered(body):
    if body.get_name() == "Player":
        if weakref(get_node(enemies)):
            get_node(enemies).visible = true
            get_node(enemies).isActive = true
