extends MarginContainer

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var canActivateSP = false
var timeStart = 0
var oneTimeOnly = true
onready var slime = get_node("Node2D/Slime")
# Called when the node enters the scene tree for the first time.
func _ready():
    pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
    timeStart+=1
    print(timeStart)
    if canActivateSP and oneTimeOnly:
        if Input.is_action_pressed("ui_select") and !get_node("Node2D/Player").is_hurt:
            slime.GRAVITY=0; slime.velocity.y = 0
            timeStart = int(delta)
            oneTimeOnly=false
        elif get_node("Node2D/Player").is_hurt:
            timeStart = int(delta)
            oneTimeOnly=false
            print("is hurrrttt")
            
    if (timeStart) == 400 and canActivateSP :
        get_node("AcceptDialog").popup_centered_ratio(0.5)
    elif (timeStart) == 300 and get_node("Node2D/Player").is_hurt:
        get_tree().change_scene("res://Scenes/GameOver.tscn")
        
func _on_Begin_pressed():
    set_process(true)
    canActivateSP=true
