extends StaticBody2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
    pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#    pass


func _on_HiddenSpikeHit_body_entered(body):
    if body.get_name() == "Player" and !body.is_hurt:
        self.visible=true
        body.is_hurt = true
        body.velocity.y = -500
