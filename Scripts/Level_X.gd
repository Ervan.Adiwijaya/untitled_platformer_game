extends Node2D

# Scroll the screen (aka. move the camera) when the character reaches the margins.
var a = 0
var gameTime = 0
# Being called when loaded.
func _ready():
    set_process(true)
        
func _process(delta):
    a += delta
    gameTime = global.time - int(a)
    if (gameTime) <= -1 :
        get_tree().change_scene(str("res://Scenes/GameOver2.tscn"))
    if get_node("Player").is_hurt:
        if Input.is_action_pressed("ui_restart"):
            global.time = gameTime
            get_tree().change_scene(str("res://Scenes/Level_X.tscn"))
        
func _physics_process(delta):
    pass